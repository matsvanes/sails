References
==========



.. rubric: References

.. [Baccala2001] Baccala L.A. and Sameshima K.  Partial directed coherence: a new concept in neural structure determination.  Biological Cybernetics. 84.  463-474.  https://doi.org/10.1007/PL00007990

.. [Barrett2010] Barrett A.B., Barnett L. and Seth A.K.  Multivariate Granger causality and generalized variance.  Physical Review E.  E81, 041907.  http://dx.doi.org/10.1103/PhysRevE.81.041907

.. [Burnham2002] Burnham K.P. and Anderson D.R.  Model Selection and Multimodel Inference: A Practical Information-Theoretic Approach.  Springer.  ISBN: 9780387224565

.. [Chatfield2003] Chatfield C.  The Analysis of Time Series: An Introduction.  Chapman and Hall.  ISBN: 9780429208706

.. [Colclough2015] Colclough G.L., Brookes M.J., Smith S.M., Woolrich M.W.  A symmetric multivariate leakage correction for MEG connectomes.  NeuroImage.  117.  439-448.  http://dx.doi.org/10.1016/j.neuroimage.2015.03.071

.. [Ding2000] Ding M., Bressler S.L., Weiming Y. and Liang H.  Short-window spectral analysis of cortical event-related potentials by adaptive multivariate autoregressive modeling: data preprocessing, model validation, and variability assessment.  Biological Cybernetics. 83(1)  35-45.  https://doi.org/10.1007/s004229900137

.. [Durbin1950] Durbin, J., & Watson, G. S. (1950). Testing For Serial Correlation In Least Squares Regression. I. Biometrika, 37(3–4), 409–428. https://doi.org/10.1093/biomet/37.3-4.409

.. [Durbin1951] Durbin, J., & Watson, G. S. (1951). Testing For Serial Correlation In Least Squares Regression. II. Biometrika, 38(1–2), 159–178. https://doi.org/10.1093/biomet/38.1-2.159

.. [Fasoula2013] Fasoula A, Attal Y and Schwartz D.  Comparative performance evaluation of data-driven causality measures applied to brain networks.  Journal of Neuroscience Methods.  215(2).  170-189.  https://doi.org/10.1016/j.jneumeth.2013.02.021

.. [Kaminski1991] Kaminski M.J. and Blinowska K.J.  A new method of the description of the information flow in the brain structures.  Biological Cybernetics. 65(3)  203-210.  http://dx.doi.org/10.1007/BF00198091

.. [Kasdin1995] Kasdin, N.J. Discrete Simulation of Colored Noise and Stochastic Processes and 1/f^\alpha Power Law Noise Generation, Proceedings of the IEEE, Vol. 83, No. 5, 1995, pp. 802-827.

.. [Korzeniewska2003] Korzeniewska A, Mańczakb M, Kamiński M, Blinowska K.J. and Kasicki S.  Determination of information flow direction among brain structures by a modified directed transfer function (dDTF) method.  Journal of Neuroscience Methods.  125(1-2).  195-207.  https://doi.org/10.1016/S0165-0270(03)00052-9

.. [Krzywinski2009] Krzywinski M, Schein J, Birol I, Connors J, Gascoyne R, Horsman D, Jones S.J. and Marra M.A.  Circos: an Information Aesthetic for Comparative Genomics.  Genome Res.  19(9).  1639-1645.  http://dx.doi.org/10.1101/gr.092759.109

.. [Lutkephol2006] Lutkepohl H.  New Introduction to Multiple Time Series Analysis.  Springer.  ISBN: 9783540262398

.. [Marple1987] Marple Jr, L.S.  Digital Spectral Analysis: With Applications.  Prentice-Hall.  ISBN: 9780132141499

.. [Neumaier2001] Neumaier A. and Schneider T.  Estimation of parameters and eigenmodes of multivariate autoregressive models.  ACM Transactions on Mathematical Software.  27(1).  27-57.  http://dx.doi.org/10.1145/382043.382304

.. [Pascual-Marqui2014] Pascual-Marqui R.D., Biscay R.J., Bosch-Bayard J., Lehmann D., Kochi K., Kinoshita T., Yamada N. and Sadato N.  Assessing direct paths of intracortical causal information flow of oscillatory activity with the isolated effective coherence (iCOH).  Frontiers in Human Neuroscience.  8.  448.  http://dx.doi.org/10.3389/fnhum.2014.00448

.. [Pascual-Marqui2017] Pascual-Marqui R.D., Biscay R.J., Bosch-Bayard J., Faber P., Kinoshita T., Kochi K., Milz P., Nishida K. and Yoshimura M.  Innovations orthogonalization: a solution to the major pitfalls of EEG/MEG "leakage correction".  Biorxiv.  https://doi.org/10.1101/178657

.. [Penny2009] Penny W.D.  Signal Processing Course.  https://www.fil.ion.ucl.ac.uk/~wpenny/course/course.html

.. [Quinn2020] Quinn A.J. and Hymers M. SAILS: Spectral Analysis In Linear Systems. Journal of Open Source Software, 5(47), 1982. https://doi.org/10.21105/joss.01982

.. [Quinn2021] Quinn A.J., Green G.G.R. and Hymers, M. Delineating between-subject heterogeneity in alpha networks with Spatio-Spectral Eigenmodes. NeuroImage, 118330. https://doi.org/10.1016/j.neuroimage.2021.118330

.. [Schelter2006] Schelter B, Winterhalder M, Eichler M, Peifer M, Hellwig B, Guschlbauer B, Hermann-Lucking C, Dahlhaus R and Timmer J.  Testing for directed influences among neural signals using partial directed coherence.  Journal of Neuroscience Methods.  152(1-2).  210-219.  https://doi.org/10.1016/j.jneumeth.2005.09.001

.. [Quirk1983] Quirk M. and Liu B.  Improving resolution for autoregressive spectral estimation by decimation.  IEEE Transactions on Acoustics, Speech and Signal Processing.  31(3)  630-637.  http://dx.doi.org/10.1109/TASSP.1983.1164124

.. [Zhu2006] Zhu M and Ghodsi A.  Automatic dimensionality selection from the scree plot via the use of profile likelihood.  Computational Statistics & Data Analysis.  51(2)  918-930.  https://doi.org/10.1016/j.csda.2005.09.010
