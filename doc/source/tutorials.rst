Tutorials
=========

The following set of tutorials will lead you through simulating data,
fitting models, exploring attributes of the models and performing
various plotting routines.

.. toctree::
   :maxdepth: 2

   tutorials/tutorial1
   tutorials/tutorial2
   tutorials/tutorial3
   tutorials/tutorial4
   tutorials/tutorial5
   tutorials/tutorial6
   tutorials/tutorial7
   tutorials/tutorial8
   tutorials/tutorial9
   tutorials/tutorial10

