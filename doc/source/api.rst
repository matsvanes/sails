API
===

The sails API can be divided up into a number of categories, each of
which has its own page:

.. toctree::
   :maxdepth: 1

   api/simulation
   api/preprocessing
   api/fitting
   api/diagnostics
   api/decomposition
   api/metrics
   api/plotting
   api/support
