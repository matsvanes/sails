Model Diagnostics
=================

Several classes and/or routines are available to help with examining and
diagnosing model fits.

.. autoclass:: sails.diags.ModelDiagnostics
   :members:

.. autoclass:: sails.diags.DelayDiagnostics
   :members:

.. autofunction:: sails.modelfit.get_residuals
